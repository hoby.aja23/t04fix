import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            try{
                System.out.print(" Pembilang : ");
                int x = scanner.nextInt();
                System.out.print(" Penyebut : ");
                int y = scanner.nextInt();
                int hasil = x / y;
                System.out.print("Hasil pembagian: " + hasil);
                validInput = true;
            }catch(ArithmeticException e){
                System.out.println("Inimah Error");
                

            }catch(InputMismatchException e){
                System.out.println("Inimah Error");
                scanner.nextLine();
            }
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int x, int y) {
        //add exception apabila penyebut bernilai 0
        if (y == 0) {
            throw new ArithmeticException("Penyebut tidak boleh nol.");
        
     }
     return x / y;
    }
}
    